
package br.com.senac.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet (urlPatterns = { "/logout" } )

public class LogoutServlet extends HttpServlet{

    @Override
    protected void doGet(HttpServletRequest requisicao, HttpServletResponse resposta) throws ServletException, IOException {
        
        HttpSession session = requisicao.getSession();
        session.invalidate();
        
        resposta.sendRedirect("home.html");
        
    }
    
    
}
