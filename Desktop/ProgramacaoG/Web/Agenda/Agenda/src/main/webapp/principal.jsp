<%-- 
    Document   : principal
    Created on : 20/08/2018, 20:43:36
    Author     :    Glauberth sala302b
    Para escrever em codigo java, usa - se "<% %>"
--%>
<%@page import="br.com.senac.agenda.model.Usuario"%>
<jsp:include page="header.jsp"/>


<div class="jumbotron">
    <h1 class="display-4">Seja bem vindo, <%= ((Usuario) session.getAttribute("user")).getNome()%> </h1>
    <p class="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
    <hr class="my-4">
    <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
    <a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a>
</div>

<jsp:include page="footer.jsp" />